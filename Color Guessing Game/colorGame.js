var isHard;
var gameStatus = document.querySelector("#gameStatus");
var squares = document.querySelectorAll(".square");
var hard = document.querySelector("#hard");
var easy = document.querySelector("#easy");
var newColors = document.querySelector("#newColors");
var requiredColor;
hard_event();


//hard
hard.addEventListener("click", hard_event);
function hard_event() {
    easy.classList.remove("selected");
    hard.classList.add("selected");
    for (var i = 0; i < squares.length; i++) {
        squares[i].classList.add("square");
        var r = Math.floor(Math.random() * 256);
        var g = Math.floor(Math.random() * 256);
        var b = Math.floor(Math.random() * 256);
        var color = "rgb(" + r + ", " + g + ", " + b + ")";
        squares[i].style.backgroundColor = color;
        squares[i].style.transition = "";
        squares[i].style.opacity = "1";
        squares[i].style.visibility = "visible";
    }
    var randomColor = Math.floor(Math.random() * squares.length);
    requiredColor = squares[randomColor].style.backgroundColor;
    document.querySelector("#requiredColor").textContent = requiredColor;
    isHard = true;
    gameStatus.textContent = "";
    newColors.textContent = "NEW COLORS";
    document.querySelector("h1").style.backgroundColor = "rgb(58, 117, 171)";
}

//easy
easy.addEventListener("click", easy_event);
function easy_event() {
    hard.classList.remove("selected");
    easy.classList.add("selected");
    for (var i = 0; i < squares.length; i++) {
        if (i < squares.length / 2) {
            squares[i].classList.add("square");
            var r = Math.floor(Math.random() * 256);
            var g = Math.floor(Math.random() * 256);
            var b = Math.floor(Math.random() * 256);
            var color = "rgb(" + r + ", " + g + ", " + b + ")";
            squares[i].style.backgroundColor = color;
            squares[i].style.transition = "";
            squares[i].style.opacity = "1";
            squares[i].style.visibility = "visible";
        } else {
            squares[i].classList.remove("square");      
        }
    }
    var randomColor = Math.floor(Math.random() * (squares.length / 2));
    requiredColor = squares[randomColor].style.backgroundColor;
    document.querySelector("#requiredColor").textContent = requiredColor;
    isHard = false;
    gameStatus.textContent = "";
    newColors.textContent = "NEW COLORS";
    document.querySelector("h1").style.backgroundColor = "rgb(58, 117, 171)";
}

//newColors
newColors.addEventListener("click", newColors_event);
function newColors_event() {
    if (isHard) {
        hard_event();
    } else {
        easy_event();
    }
}

//square
add_square_event();
function add_square_event() {
    for (var i = 0; i < squares.length; i++) {
        squares[i].addEventListener("click", function(){
            if (this.style.backgroundColor === requiredColor) {
                document.querySelector("h1").style.backgroundColor = requiredColor;
                for (var j = 0; j < squares.length; j++) {
                    squares[j].style.backgroundColor = requiredColor;
                }
                gameStatus.textContent = "Correct!";
                newColors.textContent = "PLAY AGAIN?";
            } else {
                this.style.transition = "visibility 0s 0.5s, opacity 0.5s linear";
                this.style.opacity = "0";
                this.style.visibility = "hidden";
                /*this.style.backgroundColor = "rgba(0, 0, 0, 0)";*/
                gameStatus.textContent = "Try Again";
            }
        });
    }
}